# my analisys of Maguire, everyone hates him, but i'm not sure he is terrible
# it seems like all MU/England is awful, not only because of Maguire


import pandas as pd

linde2019 = pd.read_csv(r'./data/lindelov-2019-2020.csv', header=1)
linde2020 = pd.read_csv(r'./data/lindelov-2020-2021.csv', header=1)
linde2021 = pd.read_csv(r'./data/lindelov-2021-2022.csv', header=1)
linde2022 = pd.read_csv(r'./data/lindelov-2022-2023.csv', header=1)

# print(linde2018.head())

# columns
# 0        Date  Day            Comp              Round    Venue         Result           Squad        Opponent Start Pos    Min  Gls  Ast   PK  PKatt   Sh  SoT  CrdY  CrdR  Touches  Tkl  Int  Blocks   xG  npxG  xAG  SCA  GCA   Cmp   Att  Cmp%  Prog  Succ  Att.1  Match Report

#  which columns I need:
# Result Squad Start Min
columns = ['Result', 'Squad', 'Start', 'Min']

# есть ли Магуайр в команде и выигрывает ли команда


linde2019 = linde2019[columns]
linde2020 = linde2020[columns]
linde2021 = linde2021[columns]
linde2022 = linde2022[columns]

manchester = linde2019[linde2019['Squad'] == 'Manchester Utd']
manchester = manchester.append(linde2020[linde2020['Squad'] == 'Manchester Utd'])
manchester = manchester.append(linde2021[linde2021['Squad'] == 'Manchester Utd'])
manchester = manchester.append(linde2022[linde2022['Squad'] == 'Manchester Utd'])

# ВСЕ МАТЧИ МЮ
# print(manchester)

muResearch = manchester

muResearch.loc[muResearch['Start'].str.contains('Y'), 'Start'] = '1'
muResearch.loc[muResearch['Start'].str.contains('N'), 'Start'] = '0'

#  win = 2, draw = 1, lost = 0
muResearch.loc[muResearch['Result'].str.contains('W'), 'Result'] = '2'
muResearch.loc[muResearch['Result'].str.contains('D'), 'Result'] = '1'
muResearch.loc[muResearch['Result'].str.contains('L'), 'Result'] = '0'

corr = muResearch['Start'].astype(float).corr(muResearch['Result'].astype(float))
# то есть взяимосвязь мала и она положительная, то есть, все же вклад в то что МЮ выиграл -15%... ну видимо и правда не очень...
# 0.1519022114735293
print(corr)

