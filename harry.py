# my analisys of Maguire, everyone hates him, but i'm not sure he is terrible
# it seems like all MU/England is awful, not only because of Maguire


import pandas as pd

harry2016 = pd.read_csv(r'./data/harry-log-2016-2017.csv', header=1)
harry2017 = pd.read_csv(r'./data/harry-log-2017-2018.csv', header=1)
harry2018 = pd.read_csv(r'./data/harry-log-2018-2019.csv', header=1)
harry2019 = pd.read_csv(r'./data/harry-log-2019-2020.csv', header=1)
harry2020 = pd.read_csv(r'./data/harry-log-2020-2021.csv', header=1)
harry2021 = pd.read_csv(r'./data/harry-log-2021-2022.csv', header=1)
harry2022 = pd.read_csv(r'./data/harry-log-2022-2023.csv', header=1)

# print(harry2018.head())

# columns
# 0        Date  Day            Comp              Round    Venue         Result           Squad        Opponent Start Pos    Min  Gls  Ast   PK  PKatt   Sh  SoT  CrdY  CrdR  Touches  Tkl  Int  Blocks   xG  npxG  xAG  SCA  GCA   Cmp   Att  Cmp%  Prog  Succ  Att.1  Match Report

#  which columns I need:
# Result Squad Start Min
columns = ['Result', 'Squad', 'Start', 'Min']

# есть ли Магуайр в команде и выигрывает ли команда


harry2016 = harry2016[columns]
harry2017 = harry2017[columns]
harry2018 = harry2018[columns]
harry2019 = harry2019[columns]
harry2020 = harry2020[columns]
harry2021 = harry2021[columns]
harry2022 = harry2022[columns]

manchester = harry2019[harry2019['Squad'] == 'Manchester Utd']
manchester = manchester.append(harry2020[harry2020['Squad'] == 'Manchester Utd'])
manchester = manchester.append(harry2021[harry2021['Squad'] == 'Manchester Utd'])
manchester = manchester.append(harry2022[harry2022['Squad'] == 'Manchester Utd'])

# ВСЕ МАТЧИ МЮ
# print(manchester)

muResearch = manchester

muResearch.loc[muResearch['Start'].str.contains('Y'), 'Start'] = '1'
muResearch.loc[muResearch['Start'].str.contains('N'), 'Start'] = '0'

#  win = 2, draw = 1, lost = 0
muResearch.loc[muResearch['Result'].str.contains('W'), 'Result'] = '2'
muResearch.loc[muResearch['Result'].str.contains('D'), 'Result'] = '1'
muResearch.loc[muResearch['Result'].str.contains('L'), 'Result'] = '0'

corr = muResearch['Start'].astype(float).corr(muResearch['Result'].astype(float))
# result -0.11005423305041374
# то есть взяимосвязь мала и она отрицательная, то есть,
print('IN MU', corr)


# Все матчи за сборную Англии
england = harry2017[harry2017['Squad'] == 'eng England']
england = england.append(harry2018[harry2018['Squad'] == 'eng England'])
# england = england.append(harry2019[harry2019['Squad'] == 'eng England'])
# england = england.append(harry2020[harry2020['Squad'] == 'eng England'])
# england = england.append(harry2021[harry2021['Squad'] == 'eng England'])
# england = england.append(harry2022[harry2022['Squad'] == 'eng England'])
#
# print(england)


engResearch = england

engResearch.loc[engResearch['Start'].str.contains('Y'), 'Start'] = '1'
engResearch.loc[engResearch['Start'].str.contains('N'), 'Start'] = '0'

#  win = 2, draw = 1, lost = 0
engResearch.loc[engResearch['Result'].str.contains('W'), 'Result'] = '2'
engResearch.loc[engResearch['Result'].str.contains('D'), 'Result'] = '1'
engResearch.loc[engResearch['Result'].str.contains('L'), 'Result'] = '0'

corr_e = engResearch['Start'].astype(float).corr(engResearch['Result'].astype(float))
# result 0.2733497598856183 (за всю игру в сборной)
# то есть взяимосвязь мала и она отрицательная, то есть,

# за 2017-2018 до того как его купили в МЮ
# 0.10050378152592111
print('In England', corr_e)


# НУ и то как он играл в лестере

leicester = harry2017[harry2017['Squad'] == 'Leicester City']
leicester = leicester.append(harry2018[harry2018['Squad'] == 'Leicester City'])
leicester = leicester.append(harry2019[harry2019['Squad'] == 'Leicester City'])

# print(leicester)

lecResearch = leicester

lecResearch.loc[lecResearch['Start'].str.contains('Y'), 'Start'] = '1'
lecResearch.loc[lecResearch['Start'].str.contains('N'), 'Start'] = '0'

#  win = 2, draw = 1, lost = 0
lecResearch.loc[lecResearch['Result'].str.contains('W'), 'Result'] = '2'
lecResearch.loc[lecResearch['Result'].str.contains('D'), 'Result'] = '1'
lecResearch.loc[lecResearch['Result'].str.contains('L'), 'Result'] = '0'


lecResearch = lecResearch.reset_index()
lecResearch = lecResearch.drop('index', axis=1)
lecResearch = lecResearch.reindex()

df1 = pd.DataFrame({
    "Result": [0],
    "Squad": ['My Lester'],
    "Start": ['0'],
    "Min": ['0'],
}, index=["76"])

lecResearch = lecResearch.append(df1)
# lecResearch.loc[len(lecResearch.index)] = lecResearch.append(['1', '111 Leicester City', '0', '0'])

print(lecResearch.info)
#
corr_l = lecResearch['Start'].astype(float).corr(lecResearch['Result'].astype(float))
print('in Leicester', corr_l)
#  В Лестере почти никакого влияния на победу или проигрыш,
#  0,05% такую промилю даже алкорадар не засечет
# -0.005110621300109392