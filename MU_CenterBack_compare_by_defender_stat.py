import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df_MU_defensive_19 = pd.read_csv(r'./data/MU-defensive-2019.csv', header=1)
df_MU_defensive_20 = pd.read_csv(r'./data/MU-defensive-2020.csv', header=1)
df_MU_defensive_21 = pd.read_csv(r'./data/MU-defensive-2021.csv', header=1)
df_MU_defensive_22 = pd.read_csv(r'./data/MU-defensive-2022.csv', header=1)

# print(df_MU_defensive.head())
# ['Player', 'Nation', 'Pos', 'Age', '90s', 'Tkl', 'TklW', 'Def 3rd',
#        'Mid 3rd', 'Att 3rd', 'Tkl.1', 'Att', 'Tkl%', 'Past', 'Blocks', 'Sh',
#        'Pass', 'Int', 'Tkl+Int', 'Clr', 'Err', 'Matches', '-9999'],

drop_columns = ['Nation', 'Pos', '90s', 'Age', 'Matches', '-9999']

df_MU_defensive_19 = df_MU_defensive_19.drop(drop_columns, axis=1)
df_MU_defensive_20 = df_MU_defensive_20.drop(drop_columns, axis=1)
df_MU_defensive_21 = df_MU_defensive_21.drop(drop_columns, axis=1)
df_MU_defensive_22 = df_MU_defensive_22.drop(drop_columns, axis=1)

columns = df_MU_defensive_19.drop('Player', axis=1).columns
# #
angles = np.linspace(0, 2 * np.pi, len(columns), endpoint=False)

# # 2019
# # Т.к. у каждого из игроков разное количество игрового времени, для радара имеет смысл взять только пересчет в среднем на 90 минут
# harry_def_19 = df_MU_defensive_19.drop('Player', axis=1).loc[0].values.flatten().tolist()
# linde_def_19 = df_MU_defensive_19.drop('Player', axis=1).loc[1].values.flatten().tolist()
# #
#
#
# fig=plt.figure(figsize=(6,6))
# ax=fig.add_subplot(111, polar=True)
#
# ax.plot(angles, harry_def_19, 'o-', color='g', linewidth=1, label='Maguire')
# ax.fill(angles, harry_def_19, alpha=0.25, color='g')
#
# ax.plot(angles, linde_def_19, 'o-', color='orange', linewidth=1, label='Lindelöf')
# ax.fill(angles, linde_def_19, alpha=0.25, color='orange')
#
# # ax.plot(angles, varane_std_19, 'o-', color='violet', linewidth=1, label='Varane')
# # ax.fill(angles, varane_std_19, alpha=0.25, color='violet')
#
# ax.set_thetagrids(angles * 180/np.pi, columns)
# plt.grid(True)
# plt.tight_layout()
# plt.legend()
# plt.title("2019 Maguire vs Lindelöf")
# plt.show()

# # 2020
# # print(df_MU_defensive_20.head())
#
# # Т.к. у каждого из игроков разное количество игрового времени, для радара имеет смысл взять только пересчет в среднем на 90 минут
# harry_def_20 = df_MU_defensive_20.drop('Player', axis=1).loc[1].values.flatten().tolist()
# linde_def_20 = df_MU_defensive_20.drop('Player', axis=1).loc[4].values.flatten().tolist()
#
# angles = np.linspace(0, 2 * np.pi, len(columns), endpoint=False)
#
#
# fig=plt.figure(figsize=(6,6))
# ax=fig.add_subplot(111, polar=True)
#
# ax.plot(angles, harry_def_20, 'o-', color='g', linewidth=1, label='Maguire')
# ax.fill(angles, harry_def_20, alpha=0.25, color='g')
#
# ax.plot(angles, linde_def_20, 'o-', color='orange', linewidth=1, label='Lindelöf')
# ax.fill(angles, linde_def_20, alpha=0.25, color='orange')
#
# # ax.plot(angles, varane_std_19, 'o-', color='violet', linewidth=1, label='Varane')
# # ax.fill(angles, varane_std_19, alpha=0.25, color='violet')
#
# ax.set_thetagrids(angles * 180/np.pi, columns)
# plt.grid(True)
# plt.tight_layout()
# plt.legend()
# plt.title("2020 Maguire vs Lindelöf")
# plt.show()
#

#
#
# # 2021
# print(df_MU_defensive_21.head(15))
# # #
# # # # Т.к. у каждого из игроков разное количество игрового времени, для радара имеет смысл взять только пересчет в среднем на 90 минут
# harry_def_21 = df_MU_defensive_21.drop('Player', axis=1).loc[3].values.flatten().tolist()
# linde_def_21 = df_MU_defensive_21.drop('Player', axis=1).loc[5].values.flatten().tolist()
# varane_def_21 = df_MU_defensive_21.drop('Player', axis=1).loc[8].values.flatten().tolist()
#
# angles = np.linspace(0, 2 * np.pi, len(columns), endpoint=False)
#
#
# fig=plt.figure(figsize=(6,6))
# ax=fig.add_subplot(111, polar=True)
#
# ax.plot(angles, harry_def_21, 'o-', color='g', linewidth=1, label='Maguire')
# ax.fill(angles, harry_def_21, alpha=0.25, color='g')
#
# ax.plot(angles, linde_def_21, 'o-', color='orange', linewidth=1, label='Lindelöf')
# ax.fill(angles, linde_def_21, alpha=0.25, color='orange')
#
# ax.plot(angles, varane_def_21, 'o-', color='violet', linewidth=1, label='Varane')
# ax.fill(angles, varane_def_21, alpha=0.25, color='violet')
#
# ax.set_thetagrids(angles * 180/np.pi, columns)
# plt.grid(True)
# plt.tight_layout()
# plt.legend()
# plt.title("2021 Maguire vs Lindelöf vs Varane")
# plt.show()

# 2022
print(df_MU_defensive_22.head(15))

# # Т.к. у каждого из игроков разное количество игрового времени, для радара имеет смысл взять только пересчет в среднем на 90 минут
martinez_def_22 = df_MU_defensive_22.drop('Player', axis=1).loc[0].values.flatten().tolist()
harry_def_22 = df_MU_defensive_22.drop('Player', axis=1).loc[1].values.flatten().tolist()
linde_def_22 = df_MU_defensive_22.drop('Player', axis=1).loc[2].values.flatten().tolist()
varane_def_22 = df_MU_defensive_22.drop('Player', axis=1).loc[9].values.flatten().tolist()

angles = np.linspace(0, 2 * np.pi, len(columns), endpoint=False)


fig=plt.figure(figsize=(6,6))
ax=fig.add_subplot(111, polar=True)

ax.plot(angles, harry_def_22, 'o-', color='g', linewidth=1, label='Maguire')
ax.fill(angles, harry_def_22, alpha=0.25, color='g')

ax.plot(angles, linde_def_22, 'o-', color='orange', linewidth=1, label='Lindelöf')
ax.fill(angles, linde_def_22, alpha=0.25, color='orange')

ax.plot(angles, varane_def_22, 'o-', color='violet', linewidth=1, label='Varane')
ax.fill(angles, varane_def_22, alpha=0.25, color='violet')
#
# ax.plot(angles, martinez_def_22, 'o-', color='blue', linewidth=1, label='Martinez')
# ax.fill(angles, martinez_def_22, alpha=0.25, color='blue')

ax.set_thetagrids(angles * 180/np.pi, columns)
plt.grid(True)
plt.tight_layout()
plt.legend()
plt.title("2022 Maguire vs Lindelöf vs Varane")
plt.show()