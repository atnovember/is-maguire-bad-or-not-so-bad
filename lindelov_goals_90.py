# my analisys of Maguire, everyone hates him, but i'm not sure he is terrible
# it seems like all MU/England is awful, not only because of Maguire


import pandas as pd

linde2019 = pd.read_csv(r'./data/lindelov-2019-2020.csv', header=1)
linde2020 = pd.read_csv(r'./data/lindelov-2020-2021.csv', header=1)
linde2021 = pd.read_csv(r'./data/lindelov-2021-2022.csv', header=1)
linde2022 = pd.read_csv(r'./data/lindelov-2022-2023.csv', header=1)

# print(linde2018.head())

# columns
# 0        Date  Day            Comp              Round    Venue         Result           Squad        Opponent Start Pos    Min  Gls  Ast   PK  PKatt   Sh  SoT  CrdY  CrdR  Touches  Tkl  Int  Blocks   xG  npxG  xAG  SCA  GCA   Cmp   Att  Cmp%  Prog  Succ  Att.1  Match Report

#  which columns I need:
# Result Squad Start Min
columns = ['Result', 'Squad', 'Venue', 'Start', 'Min']

# есть ли Магуайр в команде и выигрывает ли команда


linde2019 = linde2019[columns]
linde2020 = linde2020[columns]
linde2021 = linde2021[columns]
linde2022 = linde2022[columns]

manchester = linde2019[linde2019['Squad'] == 'Manchester Utd']
manchester = manchester.append(linde2020[linde2020['Squad'] == 'Manchester Utd'])
manchester = manchester.append(linde2021[linde2021['Squad'] == 'Manchester Utd'])
manchester = manchester.append(linde2022[linde2022['Squad'] == 'Manchester Utd'])

# ВСЕ МАТЧИ МЮ
# print(manchester)

muResearch = manchester
muResearch.reset_index(inplace=True)


muResearch['Goals'] = muResearch['Result'].str.split(' ', expand=True)[1]
muResearch['HomeTeamGoals'] = muResearch['Goals'].str.split("–", expand=True)[0] # works
muResearch['AwayTeamGoals'] = muResearch['Goals'].str.split("–", expand=True)[1] # works


for idx in muResearch.index:
    if muResearch['Venue'][idx] == 'Home':
        muResearch['Goals.Scored'] = muResearch['HomeTeamGoals']
        muResearch['Goals.Lost'] = muResearch['AwayTeamGoals']
    else:
        muResearch['Goals.Scored'] = muResearch['AwayTeamGoals']
        muResearch['Goals.Lost'] = muResearch['HomeTeamGoals']


# print(muResearch)
mu_corr_min_scored = muResearch['Min'].astype(float).corr(muResearch['Goals.Scored'].astype(float))
mu_corr_min_lost = muResearch['Min'].astype(float).corr(muResearch['Goals.Lost'].astype(float))

print('mu_corr_min_scored', mu_corr_min_scored)
print('mu_corr_min_lost', mu_corr_min_lost)
# mu_corr_min_scored -0.09247120890357276
# mu_corr_min_lost -0.266740183502239
