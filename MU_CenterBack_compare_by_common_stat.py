import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df_MU_19 = pd.read_csv(r'./data/MU-2019.csv', header=1)
df_MU_20 = pd.read_csv(r'./data/MU-2020.csv', header=1)
df_MU_21 = pd.read_csv(r'./data/MU-2021.csv', header=1)
df_MU_22 = pd.read_csv(r'./data/MU-2022.csv', header=1)


drop_columns = ['Nation', 'Pos', 'Age', 'MP', 'Starts', 'Min', '90s', 'Gls', 'Ast', 'G-PK', 'PK', 'PKatt', 'CrdY', 'CrdR', 'xG', 'npxG', 'xAG', 'npxG+xAG', 'Matches', '-9999']

df_MU_19 = df_MU_19.drop(drop_columns, axis=1)
df_MU_20 = df_MU_20.drop(drop_columns, axis=1)
df_MU_21 = df_MU_21.drop(drop_columns, axis=1)
df_MU_22 = df_MU_22.drop(drop_columns, axis=1)

print(df_MU_22)

columns_std = df_MU_19.drop('Player', axis=1).columns


# Т.к. у каждого из игроков разное количество игрового времени, для радара имеет смысл взять только пересчет в среднем на 90 минут
harry_std_19 = df_MU_19.drop('Player', axis=1).loc[0].values.flatten().tolist()
linde_std_19 = df_MU_19.drop('Player', axis=1).loc[1].values.flatten().tolist()

harry_std_20 = df_MU_20.drop('Player', axis=1).loc[10].values.flatten().tolist()
linde_std_20 = df_MU_20.drop('Player', axis=1).loc[14].values.flatten().tolist()

harry_std_21 = df_MU_21.drop('Player', axis=1).loc[3].values.flatten().tolist()
linde_std_21 = df_MU_21.drop('Player', axis=1).loc[5].values.flatten().tolist()
varane_std_21 = df_MU_21.drop('Player', axis=1).loc[8].values.flatten().tolist()

harry_std_22 = df_MU_22.drop('Player', axis=1).loc[18].values.flatten().tolist()
linde_std_22 = df_MU_22.drop('Player', axis=1).loc[17].values.flatten().tolist()
varane_std_22 = df_MU_22.drop('Player', axis=1).loc[8].values.flatten().tolist()


angles = np.linspace(0, 2 * np.pi, len(columns_std), endpoint=False)


# 2019 HARRY VS VICTOR
# fig=plt.figure(figsize=(6,6))
# ax=fig.add_subplot(111, polar=True)
#
# ax.plot(angles, harry_std_19, 'o-', color='g', linewidth=1, label='Maguire')
# ax.fill(angles, harry_std_19, alpha=0.25, color='g')
#
# ax.plot(angles, linde_std_19, 'o-', color='orange', linewidth=1, label='Lindelöf')
# ax.fill(angles, linde_std_19, alpha=0.25, color='orange')
#
# ax.set_thetagrids(angles * 180/np.pi, columns_std)
# plt.grid(True)
# plt.tight_layout()
# plt.legend()
# plt.title("2019 Maguire vs Lindelöf")
# plt.show()
#
# # 2020 TWO HARRY VS VICTOR
#
# fig=plt.figure(figsize=(6,6))
# ax=fig.add_subplot(111, polar=True)
#
# ax.plot(angles, harry_std_20, 'o-', color='g', linewidth=1, label='Maguire')
# ax.fill(angles, harry_std_20, alpha=0.25, color='g')
#
# ax.plot(angles, linde_std_20, 'o-', color='orange', linewidth=1, label='Lindelöf')
# ax.fill(angles, linde_std_20, alpha=0.25, color='orange')
#
# ax.set_thetagrids(angles * 180/np.pi, columns_std)
# plt.grid(True)
# plt.tight_layout()
# plt.legend()
# plt.title("2020 Maguire vs Lindelöf")
# plt.show()
#
#
# # 2021 THREE HARRY VS VICTOR VS VARANE

fig=plt.figure(figsize=(6,6))
ax=fig.add_subplot(111, polar=True)

ax.plot(angles, harry_std_21, 'o-', color='g', linewidth=1, label='Maguire')
ax.fill(angles, harry_std_21, alpha=0.25, color='g')

ax.plot(angles, linde_std_21, 'o-', color='orange', linewidth=1, label='Lindelöf')
ax.fill(angles, linde_std_21, alpha=0.25, color='orange')

ax.plot(angles, varane_std_21, 'o-', color='violet', linewidth=1, label='Varane')
ax.fill(angles, varane_std_21, alpha=0.25, color='violet')

ax.set_thetagrids(angles * 180/np.pi, columns_std)
plt.grid(True)
plt.tight_layout()
plt.legend()
plt.title("2021 Maguire vs Lindelöf vs Varane")
plt.show()

# 2022 THREE HARRY VS VICTOR VS VARANE
#
# fig=plt.figure(figsize=(6,6))
# ax=fig.add_subplot(111, polar=True)
#
# ax.plot(angles, harry_std_22, 'o-', color='g', linewidth=1, label='Maguire')
# ax.fill(angles, harry_std_22, alpha=0.25, color='g')
#
# ax.plot(angles, linde_std_22, 'o-', color='orange', linewidth=1, label='Lindelöf')
# ax.fill(angles, linde_std_22, alpha=0.25, color='orange')
#
# ax.plot(angles, varane_std_22, 'o-', color='violet', linewidth=1, label='Varane')
# ax.fill(angles, varane_std_22, alpha=0.25, color='violet')
#
# ax.set_thetagrids(angles * 180/np.pi, columns_std)
# plt.grid(True)
# plt.tight_layout()
# plt.legend()
# plt.title("2022 Maguire vs Lindelöf vs Varane")
# plt.show()
