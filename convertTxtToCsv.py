# my analisys of Maguire, everyone hates him, but i'm not sure he is terrible
# it seems like all MU/England is awful, not only because of Maguire

#
# import pandas as pd
#
# harry2018 = pd.read_csv(r'./data/harry-log-2018-2019.csv', header=1)
# harry2019 = pd.read_csv(r'./data/harry-log-2019-2020.csv', header=1)
# harry2020 = pd.read_csv(r'./data/harry-log-2020-2021.csv', header=1)
# harry2021 = pd.read_csv(r'./data/harry-log-2021-2022.csv', header=1)
# harry2022 = pd.read_csv(r'./data/harry-log-2022-2023.csv', header=1)

import glob
import pandas as pd

txt_files = glob.glob("data/*.txt")

for file in txt_files:
    # print('>>', file.split("."))
    filename = file.split(".")[0]
    print('>>>', filename)
    read_file = pd.read_csv(file)
    read_file.to_csv(filename + '.csv', index=None)