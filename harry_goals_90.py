# my analisys of Maguire, everyone hates him, but i'm not sure he is terrible
# it seems like all MU/England is awful, not only because of Maguire


import pandas as pd
import numpy as np

harry2016 = pd.read_csv(r'./data/harry-log-2016-2017.csv', header=1)
harry2017 = pd.read_csv(r'./data/harry-log-2017-2018.csv', header=1)
harry2018 = pd.read_csv(r'./data/harry-log-2018-2019.csv', header=1)
harry2019 = pd.read_csv(r'./data/harry-log-2019-2020.csv', header=1)
harry2020 = pd.read_csv(r'./data/harry-log-2020-2021.csv', header=1)
harry2021 = pd.read_csv(r'./data/harry-log-2021-2022.csv', header=1)
harry2022 = pd.read_csv(r'./data/harry-log-2022-2023.csv', header=1)

# print(harry2018.head())
# print(harry2018['Venue'].unique())

# columns
# 0        Date  Day            Comp              Round    Venue         Result           Squad        Opponent Start Pos    Min  Gls  Ast   PK  PKatt   Sh  SoT  CrdY  CrdR  Touches  Tkl  Int  Blocks   xG  npxG  xAG  SCA  GCA   Cmp   Att  Cmp%  Prog  Succ  Att.1  Match Report

#  which columns I need:
# Result Squad Start Min
columns = ['Result', 'Squad', 'Venue', 'Start', 'Min']

# есть ли Магуайр в команде и выигрывает ли команда


harry2016 = harry2016[columns]
harry2017 = harry2017[columns]
harry2018 = harry2018[columns]
harry2019 = harry2019[columns]
harry2020 = harry2020[columns]
harry2021 = harry2021[columns]
harry2022 = harry2022[columns]

manchester = harry2019[harry2019['Squad'] == 'Manchester Utd']
manchester = manchester.append(harry2020[harry2020['Squad'] == 'Manchester Utd'])
manchester = manchester.append(harry2021[harry2021['Squad'] == 'Manchester Utd'])
manchester = manchester.append(harry2022[harry2022['Squad'] == 'Manchester Utd'])

# ВСЕ МАТЧИ МЮ
# print(manchester)

muResearch = manchester
muResearch.reset_index(inplace=True)

# проанализируем как количество игровых минут влияет на то сколько голов команда пропустила, и сколько голов забила
# для этого надо разделить на 2 колонки результат Balls.Scored, Balls.Lost


muResearch['Goals'] = muResearch['Result'].str.split(' ', expand=True)[1]
muResearch['HomeTeamGoals'] = muResearch['Goals'].str.split("–", expand=True)[0] # works
muResearch['AwayTeamGoals'] = muResearch['Goals'].str.split("–", expand=True)[1] # works


for idx in muResearch.index:
    if muResearch['Venue'][idx] == 'Home':
        muResearch['Goals.Scored'] = muResearch['HomeTeamGoals']
        muResearch['Goals.Lost'] = muResearch['AwayTeamGoals']
    else:
        muResearch['Goals.Scored'] = muResearch['AwayTeamGoals']
        muResearch['Goals.Lost'] = muResearch['HomeTeamGoals']


# print(muResearch)
mu_corr_min_scored = muResearch['Min'].astype(float).corr(muResearch['Goals.Scored'].astype(float))
mu_corr_min_lost = muResearch['Min'].astype(float).corr(muResearch['Goals.Lost'].astype(float))

print('mu_corr_min_scored', mu_corr_min_scored)
print('mu_corr_min_lost', mu_corr_min_lost)
# mu_corr_min_scored -0.09247120890357276
# mu_corr_min_lost -0.266740183502239

#
# # Все матчи за сборную Англии
england = harry2017[harry2017['Squad'] == 'eng England']
england = england.append(harry2018[harry2018['Squad'] == 'eng England'])
# england = england.append(harry2019[harry2019['Squad'] == 'eng England'])
# england = england.append(harry2020[harry2020['Squad'] == 'eng England'])
# england = england.append(harry2021[harry2021['Squad'] == 'eng England'])
# england = england.append(harry2022[harry2022['Squad'] == 'eng England'])
#
# print(england)


engResearch = england
engResearch.reset_index(inplace=True)
# engResearch.drop('index', axis=True)
# проанализируем как количество игровых минут влияет на то сколько голов команда пропустила, и сколько голов забила
# для этого надо разделить на 2 колонки результат Balls.Scored, Balls.Lost


engResearch['Goals'] = engResearch['Result'].str.split(' ', expand=True)[1]
# engResetIndex = engResearch[ (engResearch['Result'].str.contains('(')) ].index
# print('engResetIndex', engResetIndex)
engResearch['HomeTeamGoals'] = engResearch['Goals'].str.split("–", expand=True)[0] # works
engResearch['AwayTeamGoals'] = engResearch['Goals'].str.split("–", expand=True)[1] # works


#  drop idx 47 and 8
# лять я заебалась, я не знаю как уебать эту ебаную строчку с ебаной скобочкой, просто нахуй уебу их по индексу. сука
# захардкодить ее к хренам собачьим
engResearch['AwayTeamGoals'].replace(to_replace=[None], value="0", inplace=True)

for idx in engResearch.index:
    if engResearch['Venue'][idx] == 'Home':
        engResearch['Goals.Scored'] = engResearch['HomeTeamGoals']
        engResearch['Goals.Lost'] = engResearch['AwayTeamGoals']
    else:
        engResearch['Goals.Scored'] = engResearch['AwayTeamGoals']
        engResearch['Goals.Lost'] = engResearch['HomeTeamGoals']


# print(engResearch)
eng_corr_min_scored = engResearch['Min'].astype(float).corr(engResearch['Goals.Scored'].astype(float))
eng_corr_min_lost = engResearch['Min'].astype(float).corr(engResearch['Goals.Lost'].astype(float))

print('eng_corr_min_scored', eng_corr_min_scored)
print('eng_corr_min_lost', eng_corr_min_lost)
# eng_corr_min_scored 0.18174972574863235
# eng_corr_min_lost -0.039667435365461086

#
# # НУ и то как он играл в лестере
#
leicester = harry2017[harry2017['Squad'] == 'Leicester City']
leicester = leicester.append(harry2018[harry2018['Squad'] == 'Leicester City'])
leicester = leicester.append(harry2019[harry2019['Squad'] == 'Leicester City'])
#
# # print(leicester)
#
lecResearch = leicester

lecResearch.reset_index(inplace=True)
# engResearch.drop('index', axis=True)
# проанализируем как количество игровых минут влияет на то сколько голов команда пропустила, и сколько голов забила
# для этого надо разделить на 2 колонки результат Balls.Scored, Balls.Lost


lecResearch['Goals'] = lecResearch['Result'].str.split(' ', expand=True)[1]
# engResetIndex = engResearch[ (engResearch['Result'].str.contains('(')) ].index
# print('engResetIndex', engResetIndex)
lecResearch['HomeTeamGoals'] = lecResearch['Goals'].str.split("–", expand=True)[0] # works
lecResearch['AwayTeamGoals'] = lecResearch['Goals'].str.split("–", expand=True)[1] # works


#  drop idx 47 and 8
# лять я заебалась, я не знаю как уебать эту ебаную строчку с ебаной скобочкой, просто нахуй уебу их по индексу. сука
# захардкодить ее к хренам собачьим
lecResearch['AwayTeamGoals'].replace(to_replace=[None], value="0", inplace=True)

for idx in lecResearch.index:
    if lecResearch['Venue'][idx] == 'Home':
        lecResearch['Goals.Scored'] = lecResearch['HomeTeamGoals']
        lecResearch['Goals.Lost'] = lecResearch['AwayTeamGoals']
    else:
        lecResearch['Goals.Scored'] = lecResearch['AwayTeamGoals']
        lecResearch['Goals.Lost'] = lecResearch['HomeTeamGoals']


# print(engResearch)
lec_corr_min_scored = lecResearch['Min'].astype(float).corr(lecResearch['Goals.Scored'].astype(float))
lec_corr_min_lost = lecResearch['Min'].astype(float).corr(lecResearch['Goals.Lost'].astype(float))

print('lec_corr_min_scored', lec_corr_min_scored)
print('lec_corr_min_lost', lec_corr_min_lost)
# lec_corr_min_scored -0.1155227453023691
# lec_corr_min_lost -0.033149361281752915
