# my analisys of Maguire, everyone hates him, but i'm not sure he is terrible
# it seems like all MU/England is awful, not only because of Maguire


import pandas as pd

# varane2019 = pd.read_csv(r'./data/varane-2019-2020.csv', header=1)
varane2020 = pd.read_csv(r'./data/varane-2020-2021.csv', header=1)
varane2021 = pd.read_csv(r'./data/varane-2021-2022.csv', header=1)
varane2022 = pd.read_csv(r'./data/varane-2022-2023.csv', header=1)

# print(varane2018.head())

# columns
# 0        Date  Day            Comp              Round    Venue         Result           Squad        Opponent Start Pos    Min  Gls  Ast   PK  PKatt   Sh  SoT  CrdY  CrdR  Touches  Tkl  Int  Blocks   xG  npxG  xAG  SCA  GCA   Cmp   Att  Cmp%  Prog  Succ  Att.1  Match Report

#  which columns I need:
# Result Squad Start Min
columns = ['Result', 'Squad', 'Start', 'Min']

# есть ли Магуайр в команде и выигрывает ли команда


# varane2019 = varane2019[columns]
varane2020 = varane2020[columns]
varane2021 = varane2021[columns]
varane2022 = varane2022[columns]

# manchester = varane2019[varane2019['Squad'] == 'Manchester Utd']
manchester = varane2020[varane2020['Squad'] == 'Manchester Utd']
manchester = manchester.append(varane2021[varane2021['Squad'] == 'Manchester Utd'])
manchester = manchester.append(varane2022[varane2022['Squad'] == 'Manchester Utd'])

# ВСЕ МАТЧИ МЮ
# print(manchester)


muResearch = manchester
muResearch.reset_index(inplace=True)

# проанализируем как количество игровых минут влияет на то сколько голов команда пропустила, и сколько голов забила
# для этого надо разделить на 2 колонки результат Balls.Scored, Balls.Lost


muResearch['Goals'] = muResearch['Result'].str.split(' ', expand=True)[1]
muResearch['HomeTeamGoals'] = muResearch['Goals'].str.split("–", expand=True)[0] # works
muResearch['AwayTeamGoals'] = muResearch['Goals'].str.split("–", expand=True)[1] # works


for idx in muResearch.index:
    if muResearch['Venue'][idx] == 'Home':
        muResearch['Goals.Scored'] = muResearch['HomeTeamGoals']
        muResearch['Goals.Lost'] = muResearch['AwayTeamGoals']
    else:
        muResearch['Goals.Scored'] = muResearch['AwayTeamGoals']
        muResearch['Goals.Lost'] = muResearch['HomeTeamGoals']


# print(muResearch)
mu_corr_min_scored = muResearch['Min'].astype(float).corr(muResearch['Goals.Scored'].astype(float))
mu_corr_min_lost = muResearch['Min'].astype(float).corr(muResearch['Goals.Lost'].astype(float))

print('mu_corr_min_scored', mu_corr_min_scored)
print('mu_corr_min_lost', mu_corr_min_lost)
# lec_corr_min_scored -0.1155227453023691
# lec_corr_min_lost -0.033149361281752915


